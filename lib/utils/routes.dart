import 'package:aycapital/utils/my_routes.dart';
import 'package:aycapital/view/Login/user_login.dart';
import 'package:aycapital/view/chart_show.dart';
import 'package:aycapital/view/percent_indicator.dart';
import 'package:aycapital/view/user_portfolio.dart';
import 'package:aycapital/view/user_profile.dart';
import 'package:get/get.dart';

routes() => [
      GetPage(
          name: MyRoutes.user_profile,
          page: () => UserProfile(),
          transitionDuration: Duration(milliseconds: 100),
          transition: Transition.zoom),
      GetPage(
          name: MyRoutes.user_login,
          page: () => UserLogin(),
          transitionDuration: Duration(milliseconds: 100),
          transition: Transition.zoom),
      GetPage(
          name: MyRoutes.chart_show,
          page: () => ChartShow(),
          transitionDuration: Duration(milliseconds: 100),
          transition: Transition.zoom),
      GetPage(
          name: MyRoutes.userProfile_portfolio,
          page: () => UserProfilePortfolio(),
          transitionDuration: Duration(milliseconds: 100),
          transition: Transition.zoom),
      GetPage(
          name: MyRoutes.percent_indicator,
          page: () => PercentIndicator(),
          transitionDuration: Duration(milliseconds: 100),
          transition: Transition.zoom),
      // GetPage(
      //     name: MyRoutes.user_screen,
      //     page: () => BarChartSample3(),
      //     transitionDuration: Duration(milliseconds: 250),
      //     transition: Transition.zoom),
    ];

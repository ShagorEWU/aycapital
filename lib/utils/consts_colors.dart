import 'package:flutter/material.dart';

class ConstColors {
  static const MaterialColor primary = MaterialColor(0xff8e7557, {
    50: Color.fromRGBO(142, 117, 87, .1),
    100: Color.fromRGBO(142, 117, 87, .2),
    200: Color.fromRGBO(142, 117, 87, .3),
    300: Color.fromRGBO(142, 117, 87, .4),
    400: Color.fromRGBO(142, 117, 87, .5),
    500: Color.fromRGBO(142, 117, 87, .6),
    600: Color.fromRGBO(142, 117, 87, .7),
    700: Color.fromRGBO(142, 117, 87, .8),
    800: Color.fromRGBO(142, 117, 87, .9),
    900: Color.fromRGBO(142, 117, 87, 1)
  });

  static const Color sliderbg = Color(0xffd2b593);
  static const Color card_border = Color(0xffd2ceca);
  static const Color card_text = Color(0xff8C765A);
  static const Color screenbg = Color(0xffFFFBF6);
  static const Color reference = Color(0xff808080);
  static const Color test = Color(0xff534034);
  static const Color item = Color.fromRGBO(140, 118, 90, 0.7);
  static const Color itemIcon = Color.fromRGBO(140, 118, 90, 0.7);
  static const Color popUpShare = Color.fromRGBO(174, 214, 241, 0.7);
  static const Color popUpMessage = Color.fromRGBO(26, 82, 118, 0.7);
  static const Color homeStatusBar = Color.fromRGBO(187, 179, 169, 0.7);
  static const Color homeAppsBar = Color.fromRGBO(142, 117, 87, 0.7);
  static const Color homeAyatSliderBackground =
      Color.fromRGBO(210, 181, 147, 0.7);
  static const Color homeScreenBackGround = Color.fromRGBO(255, 251, 246, 0.7);
  static const Color homeCardBackGround = Colors.white;
  static const Color homeCardBorder = Color.fromRGBO(210, 206, 202, 0.7);
  static const Color homeCardText = Color.fromRGBO(140, 118, 90, 0.7);
  static const Color questionBorder = Color.fromRGBO(209, 195, 167, 0.7);
}

import 'package:flutter/material.dart';

class Stack2 extends StatefulWidget {
  Stack2({Key key}) : super(key: key);

  @override
  _Stack2State createState() => _Stack2State();
}

class _Stack2State extends State<Stack2> {
  @override
  Widget build(BuildContext context) {
    return Container(
        // color: Colors.red,
        // width: MediaQuery.of(context).size.width - 32,
        height: 50,
        child: Row(children: [
          // Padding(padding: const EdgeInsets.all(2.5)),
          Container(
            width: ((MediaQuery.of(context).size.width - 32) / 4.0) + 25,
            child: Stack(children: [
              Positioned(
                left: 3,
                top: 12,
                child: Icon(
                  Icons.people,
                  color: Colors.white,
                ),
              ),
              Positioned(
                left: 30,
                top: 9,
                child: Text(
                  "Shagor",
                  style: TextStyle(fontSize: 12, color: Colors.white),
                ),
              ),
              Positioned(
                left: 30,
                bottom: 9,
                child: Text(
                  "Lead Designer",
                  style: TextStyle(fontSize: 12, color: Colors.white),
                ),
              ),
            ]),
          ),
          // Padding(padding: const EdgeInsets.all(5)),
          Container(
            width: ((MediaQuery.of(context).size.width - 32) / 4.0) - 25,
            child: Center(
              //alignment: Alignment.topLeft,
              child: Text("\$800000",
                  style: TextStyle(fontSize: 10, color: Colors.white)),
            ),
          ),
          Container(
            width: ((MediaQuery.of(context).size.width - 32) / 4.0) + 12,
            child: Center(
              child: Text("14 October, 2021",
                  style: TextStyle(fontSize: 10, color: Colors.white)),
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5),
            ),
            height: 20,
            width: ((MediaQuery.of(context).size.width - 32) / 4.0) - 12,
            child: Center(
              child: Text("InProgress",
                  style: TextStyle(
                      fontSize: 12,
                      color: Colors.green,
                      fontWeight: FontWeight.bold)),
            ),
          )
        ]));
  }
}

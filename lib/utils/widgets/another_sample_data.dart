import 'package:aycapital/utils/widgets/hexcolor.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class BarChartSample3 extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => BarChartSample3State();
}

class BarChartSample3State extends State<BarChartSample3> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 208,
      // padding: const EdgeInsets.all(13.0),
      child: Container(
        decoration: BoxDecoration(
          color: HexColor("#232323"),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                    height: 15,
                    child: Text(
                      "Traffic Source Status",
                      style: TextStyle(color: Colors.white, fontSize: 12),
                    )),
              ),
              Padding(padding: const EdgeInsets.all(5)),
              Container(
                height: 130,
                child: Row(
                  children: [
                    Align(
                        alignment: Alignment.centerLeft,
                        child: new RotatedBox(
                            quarterTurns: -1,
                            child: new Text(
                              "Total Percentage Market Share",
                              style:
                                  TextStyle(fontSize: 8, color: Colors.white),
                            ))),
                    Container(
                      child: Align(
                          alignment: Alignment.centerLeft,
                          child: new RotatedBox(
                              quarterTurns: -1,
                              child: new Text(
                                "             ",
                                style: TextStyle(
                                    fontSize: 10, color: Colors.white),
                              ))),
                    ),
                    BarChart(
                      BarChartData(
                        // groupsSpace: 100.0,
                        // groupsSpace: 20,
                        // alignment: BarChartAlignment.spaceAround,
                        maxY: 100.0,
                        barTouchData: BarTouchData(
                          enabled: false,
                          touchTooltipData: BarTouchTooltipData(
                            tooltipBgColor: Colors.transparent,
                            tooltipPadding: const EdgeInsets.all(0),
                            tooltipBottomMargin: 8,
                            getTooltipItem: (
                              BarChartGroupData group,
                              int groupIndex,
                              BarChartRodData rod,
                              int rodIndex,
                            ) {
                              return BarTooltipItem(
                                rod.y.round().toString(),
                                TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              );
                            },
                          ),
                        ),
                        titlesData: FlTitlesData(
                          show: true,
                          bottomTitles: SideTitles(
                            showTitles: true,
                            getTextStyles: (value) => const TextStyle(
                                color: Color(0xff7589a2),
                                fontWeight: FontWeight.bold,
                                fontSize: 12),
                            margin: 25,
                            getTitles: (double value) {
                              switch (value.toInt()) {
                                case 0:
                                  return 'Organic Search';
                                case 1:
                                  return 'Direct';
                                case 2:
                                  return 'Referrel';

                                default:
                                  return 'Others';
                              }
                            },
                          ),
                          leftTitles: SideTitles(
                              getTextStyles: (value) => const TextStyle(
                                  color: Color(0xff7589a2),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14),
                              margin: 20,
                              getTitles: (double value) {
                                switch (value.toInt()) {
                                  case 0:
                                    return '0';
                                  case 50:
                                    return '50';
                                  case 100:
                                    return '100';

                                  default:
                                    return '';
                                }
                              },
                              showTitles: true),
                        ),
                        borderData: FlBorderData(
                          show: false,
                        ),
                        barGroups: [
                          BarChartGroupData(
                            barsSpace: 20,
                            x: 0,
                            barRods: [
                              BarChartRodData(
                                  borderRadius: BorderRadius.circular(0),
                                  width: 20,
                                  y: 62.5,
                                  colors: [Colors.blue])
                            ],
                            showingTooltipIndicators: [0],
                          ),
                          BarChartGroupData(
                            barsSpace: 20,
                            x: 1,
                            barRods: [
                              BarChartRodData(
                                  borderRadius: BorderRadius.circular(0),
                                  width: 20,
                                  y: 40.5,
                                  colors: [Colors.red])
                            ],
                            showingTooltipIndicators: [0],
                          ),
                          BarChartGroupData(
                            barsSpace: 20,
                            x: 2,
                            barRods: [
                              BarChartRodData(
                                  borderRadius: BorderRadius.circular(0),
                                  width: 20,
                                  y: 25.5,
                                  colors: [Colors.green])
                            ],
                            showingTooltipIndicators: [0],
                          ),
                          BarChartGroupData(
                            barsSpace: 20,
                            x: 3,
                            barRods: [
                              BarChartRodData(
                                  borderRadius: BorderRadius.circular(0),
                                  width: 20,
                                  y: 12,
                                  colors: [Colors.red[100]])
                            ],
                            showingTooltipIndicators: [0],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

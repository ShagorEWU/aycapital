import 'package:flutter/material.dart';

class Stack3 extends StatefulWidget {
  Stack3({Key key}) : super(key: key);

  @override
  _Stack3State createState() => _Stack3State();
}

class _Stack3State extends State<Stack3> {
  @override
  Widget build(BuildContext context) {
    return Container(
        // color: Colors.red,
        // width: MediaQuery.of(context).size.width - 32,
        height: 50,
        child: Row(children: [
          // Padding(padding: const EdgeInsets.all(2.5)),
          Container(
            width: ((MediaQuery.of(context).size.width - 32) / 4.0) + 25,
            child: Stack(children: [
              Positioned(
                left: 3,
                top: 12,
                child: Icon(
                  Icons.people,
                  color: Colors.white,
                ),
              ),
              Positioned(
                left: 30,
                top: 9,
                child: Text(
                  "Shagor",
                  style: TextStyle(fontSize: 12, color: Colors.white),
                ),
              ),
              Positioned(
                left: 30,
                bottom: 9,
                child: Text(
                  "Lead Designer",
                  style: TextStyle(fontSize: 12, color: Colors.white),
                ),
              ),
            ]),
          ),
          // Padding(padding: const EdgeInsets.all(5)),
          Container(
            width: ((MediaQuery.of(context).size.width - 32) / 4.0) - 25,
            child: Center(
              //alignment: Alignment.topLeft,
              child: Text("\$800000",
                  style: TextStyle(fontSize: 10, color: Colors.white)),
            ),
          ),
          Container(
            width: ((MediaQuery.of(context).size.width - 32) / 4.0) + 12,
            child: Center(
              child: Text("14 October, 2021",
                  style: TextStyle(fontSize: 10, color: Colors.white)),
            ),
          ),
          Container(
            height: 20,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5),
            ),
            width: ((MediaQuery.of(context).size.width - 32) / 4.0) - 12,
            child: Center(
              child: Text("Cancel",
                  style: TextStyle(
                      fontSize: 12,
                      color: Colors.red,
                      fontWeight: FontWeight.bold)),
            ),
          )
        ]));
  }
}

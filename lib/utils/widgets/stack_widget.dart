import 'package:flutter/material.dart';

class Stack1 extends StatefulWidget {
  Stack1({Key key}) : super(key: key);

  @override
  _Stack1State createState() => _Stack1State();
}

class _Stack1State extends State<Stack1> {
  @override
  void initState() {
    // print(MediaQuery.of(context).size.width);
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        // color: Colors.red,
        // width: MediaQuery.of(context).size.width - 32,
        height: 50,
        child: Row(children: [
          // Padding(padding: const EdgeInsets.all(2.5)),
          Container(
            width: ((MediaQuery.of(context).size.width - 32) / 4.0) + 25,
            child: Stack(children: [
              Positioned(
                left: 3,
                top: 12,
                child: Icon(
                  Icons.people,
                  color: Colors.white,
                ),
              ),
              Positioned(
                left: 30,
                top: 9,
                child: Text(
                  "Shagor",
                  style: TextStyle(fontSize: 12, color: Colors.white),
                ),
              ),
              Positioned(
                left: 30,
                bottom: 9,
                child: Text(
                  "Lead Designer",
                  style: TextStyle(fontSize: 12, color: Colors.white),
                ),
              ),
            ]),
          ),
          // Padding(padding: const EdgeInsets.all(5)),
          Container(
            width: ((MediaQuery.of(context).size.width - 32) / 4.0) - 25,
            child: Center(
              //alignment: Alignment.topLeft,
              child: Text("\$800000",
                  style: TextStyle(fontSize: 10, color: Colors.white)),
            ),
          ),
          Container(
            width: ((MediaQuery.of(context).size.width - 32) / 4.0) + 12,
            child: Center(
              child: Text("14 October, 2021",
                  style: TextStyle(fontSize: 10, color: Colors.white)),
            ),
          ),
          Container(
            height: 20,
            // color: Colors.white,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5),
            ),
            width: ((MediaQuery.of(context).size.width - 32) / 4.0) - 12,
            child: Center(
              child: Text("Completed",
                  style: TextStyle(
                      fontSize: 12,
                      color: Colors.black,
                      fontWeight: FontWeight.bold)),
            ),
          )
        ]));
  }
}

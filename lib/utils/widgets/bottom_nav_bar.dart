import 'package:aycapital/utils/my_routes.dart';
import 'package:aycapital/utils/widgets/hexcolor.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
//import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';

BuildContext testContext;

class BottomNavBar extends StatefulWidget {
  const BottomNavBar({
    Key key,
    @required this.activeBar,
  }) : super(key: key);

  final int activeBar;
  @override
  _BottomNavBarState createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  int _currentIndex;
  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
    _currentIndex == 0
        ? Get.toNamed(MyRoutes.chart_show)
        : _currentIndex == 3
            ? Get.toNamed(MyRoutes.user_profile)
            : _currentIndex == 2
                ? Get.toNamed(MyRoutes.percent_indicator)
                : Get.toNamed(MyRoutes.userProfile_portfolio);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _currentIndex = widget.activeBar;
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
          // sets the background color of the `BottomNavigationBar`
          //  canvasColor: HexColor("#34495E"),
          canvasColor: HexColor("#232323"),
          // sets the active color of the `BottomNavigationBar` if `Brightness` is light
          primaryColor: Colors.black,
          textTheme: Theme.of(context)
              .textTheme
              .copyWith(caption: new TextStyle(color: Colors.yellow))),
      child: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        // backgroundColor: HexColor("232323"),
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.grey,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "DashBoard",
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.account_tree, semanticLabel: "My Portfolio"),
              label: "My Portfolio"),
          BottomNavigationBarItem(
              icon: Icon(Icons.pie_chart), label: "Targets"),
          BottomNavigationBarItem(
              icon: Icon(Icons.person, semanticLabel: "Profile"),
              label: "Profile"),
          // BottomNavigationBarItem(icon: Icon(Icons.people), label: "Profile"),
        ],
      ),
    );
  }
}

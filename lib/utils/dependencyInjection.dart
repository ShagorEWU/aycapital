import 'package:aycapital/API/Provider/LoginApiProvider.dart';
import 'package:aycapital/API/Provider/RegistrationApiProvider.dart';
import 'package:aycapital/API/Repository/LoginRepository.dart';
import 'package:aycapital/API/Repository/RegistrationRepository.dart';
import 'package:aycapital/utils/Notifications.dart';
import 'package:aycapital/utils/Storage/storagePref.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class DependencyInjection implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<Notifications>(() => Notifications(), fenix: true);

    Get.lazyPut<LoginRepository>(() => LoginRepository(), fenix: true);
    Get.lazyPut<LoginApiProvider>(() => LoginApiProvider(), fenix: true);
    Get.lazyPut<RegistrationRepository>(() => RegistrationRepository(),
        fenix: true);
    Get.lazyPut<RegistrationApiProvider>(() => RegistrationApiProvider(),
        fenix: true);
    Get.lazyPut<GetStorage>(() => GetStorage(), fenix: true);
    Get.lazyPut<StoragePref>(() => StoragePref(), fenix: true);

    // Get.create<ItemController>(() => ItemController(),permanent: true);

    //BottomShowController icontroller = Get.put(BottomShowController());
  }
}

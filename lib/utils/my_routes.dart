class MyRoutes {
  static const String home_page = "/";
  static const String answer_page = "/answer_page";
  static const String fikah_zakat_details_page = "/fikah_zakat_details_page";
  static const String fikah_zakat_page = "/fikah_zakat_page";
  static const String questions_page = "/questions_page";
  static const String zakat_organizations_page = "/zakat_organizations_page";
  static const String zakat_organization_details_page =
      "/zakat_organization_details_page";
  static const String calculator_page = '/calculator_page';
  static const String calculator_zakat_category_page =
      '/calculator_zakat_category_page';

  static const String calculator_add_field_page = '/calculator_add_field_page';
  static const String calculator_zakat_details_page =
      '/calculator_zakat_details_page';
  static const String calculator_add_field_gold_page =
      '/calculator_add_field_gold_page';
  static const String search_page = '/search_page';
  static const String jakater_hisheb_page = '/jakater_hisheb_page';
  static const String user_profile = '/user_profile';
  static const String user_login = '/user_login';
  static const String percent_indicator = '/percent_indicator';
  static const String user_screen = '/user_screen';
  static const String userProfile_portfolio = '/userProfile_portfolio';
  static const String chart_show = '/chart_show';
}

import 'dart:io';

import 'package:aycapital/utils/dependencyInjection.dart';
import 'package:aycapital/utils/my_routes.dart';
import 'package:aycapital/utils/routes.dart';
import 'package:aycapital/utils/widgets/hexcolor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: Platform.isAndroid
            ? SystemUiOverlayStyle.dark.copyWith(
                // To make Status bar icons color white in Android devices.
                statusBarIconBrightness: Brightness.light,
                statusBarColor: HexColor("#121212"),
                systemNavigationBarColor: Colors.black12,
                systemNavigationBarIconBrightness: Brightness.dark,
              )
            : SystemUiOverlayStyle.dark.copyWith(
                // statusBarBrightness is used to set Status bar icon color in iOS.
                statusBarBrightness: Brightness.light,
                statusBarColor: HexColor("#121212"),
                systemNavigationBarColor: Colors.black12,
                systemNavigationBarIconBrightness: Brightness.dark,
              ),
        child: GetMaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            //scaffoldBackgroundColor: Color.fromRGBO(35, 35, 35, 0.7),
            // primarySwatch: ConstColors.primary,
            fontFamily: "kalpurush",
          ),
          initialBinding: DependencyInjection(),
          // home: ContertPage2(),
          initialRoute: MyRoutes.user_login,
          getPages: routes(),
          // transitionDuration: Duration(milliseconds: 300),
        ));
  }
}

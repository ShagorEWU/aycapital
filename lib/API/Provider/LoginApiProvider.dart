import 'dart:async';

import 'package:aycapital/Model/loginModel/LoginModel.dart';
import 'package:dio/dio.dart';

class LoginApiProvider {
  Dio dio = new Dio();

  Future checkLogin(Map<String, dynamic> map) async {
    try {
      Response response =
          await dio.post('http://aycapital.sarosit.com/api/login', data: map);
      print(response.data);
      return LoginModel.fromJson(response.data);
    } //The model class returns data with json.decode().
    on DioError catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return LoginModel.fromJson({
        "success": false,
        "data": {
          "token": "46|6wFCUNya0NwWGuTvMbxFor8AyaNYuoiAETPp7rUY",
          "user": {
            "id": 15,
            "name": "saghor",
            "email": "123@gmail.com",
            "email_verified_at": null,
            "current_team_id": null,
            "profile_photo_path": null,
            "provider": null,
            "provider_id": null,
            "created_at": "2021-03-23T20:41:00.000000Z",
            "updated_at": "2021-03-23T20:41:00.000000Z",
            "profile_photo_url":
                "https://ui-avatars.com/api/?name=saghor&color=7F9CF5&background=EBF4FF"
          }
        },
        "message": "Login success."
      });
    }
  }
}

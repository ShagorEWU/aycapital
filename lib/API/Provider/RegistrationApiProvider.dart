import 'dart:async';

import 'package:aycapital/Model/RegistrationModel/RegistrationModel.dart';
import 'package:dio/dio.dart';

class RegistrationApiProvider {
  Dio dio = new Dio();

  Future checkRegistration(Map<String, dynamic> map) async {
    try {
      Response response = await dio
          .post('https://aycapital.sarosit.com/api/register', data: map);

      return RegistrationModel.fromJson(response.data);
      //The model class returns data with json.decode().
    } on DioError catch (error, stacktrace) {
      print("Exception occured: $error stackTrace: $stacktrace");
      return RegistrationModel.fromJson({
        "success": false,
        "data": {
          "token": "23|g3k4QOzAb4qf5KUWK6RUTQFMUCaBS97LZMyPalc7",
          "user": {
            "name": "sagore",
            "email": "abdullahalnoman3798@gmail.com",
            "updated_at": "2021-03-23T11:41:10.000000Z",
            "created_at": "2021-03-23T11:41:10.000000Z",
            "id": 11,
            "profile_photo_url":
                "https://ui-avatars.com/api/?name=sagore&color=7F9CF5&background=EBF4FF",
            "roles": [
              {
                "id": 3,
                "name": "investor",
                "guard_name": "web",
                "created_at": "2021-03-21T07:49:47.000000Z",
                "updated_at": "2021-03-21T07:49:47.000000Z",
                "pivot": {
                  "model_id": "11",
                  "role_id": "3",
                  "model_type": "App\\Models\\User"
                }
              }
            ]
          }
        },
        "message": "User created success."
      });
    }
  }
}

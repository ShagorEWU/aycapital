import 'dart:async';
import 'package:dio/dio.dart';
import 'package:dio/dio.dart' as dio;
import 'package:flutter/foundation.dart' as Foundation;

class ApiConf {
  Dio dio;

  ApiConf() {
    if (dio == null) {
      Map<String, String> reqHeaders = {
        "Content-type": "application/x-www-form-urlencoded",
        "Accept": "application/json",
        "Authorization": ""
      };

      BaseOptions options = BaseOptions(
          baseUrl: "http://api.joltop.com/rocketmealapi/",
          receiveTimeout: 5 * 60 * 1000, //Minutes
          connectTimeout: 10 * 1000, //Seconds
          headers: reqHeaders,
          contentType: Headers.formUrlEncodedContentType);

      dio = new Dio(options);
      if (Foundation.kDebugMode) {
        //Debug Mode
        dio.interceptors.add(LoggingInterceptors());
      }
    }
  }

  Dio getDio() {
    return dio;
  }

  String handleError(DioError dioError) {
    String errorDescription = "";

    switch (dioError.type) {
      case DioErrorType.CANCEL:
        errorDescription = "Request to API server was cancelled";
        break;
      case DioErrorType.CONNECT_TIMEOUT:
        errorDescription = "Connection timeout with API server";
        break;
      case DioErrorType.DEFAULT:
        errorDescription = "Connection to API server failed";
        break;
      case DioErrorType.RECEIVE_TIMEOUT:
        errorDescription = "Receive timeout in connection with API server";
        break;
      case DioErrorType.RESPONSE:
        errorDescription =
            "Received invalid status code: ${dioError.response.statusCode}";
        break;
      case DioErrorType.SEND_TIMEOUT:
        errorDescription = "Send timeout in connection with API server";
        break;

      default:
        errorDescription = "Unknown Error";
        break;
    }

    return errorDescription;
  }
}

class LoggingInterceptors extends Interceptor {
  @override
  Future<FutureOr> onRequest(RequestOptions options) async {
    print(
        "\n\n--> ${options.method != null ? options.method.toUpperCase() : 'METHOD'} ${"" + (options.baseUrl ?? "") + (options.path ?? "")}");
    print("Headers:");
    options.headers.forEach((k, v) => print('$k: $v'));
    if (options.queryParameters != null) {
      print("queryParameters:");
      options.queryParameters.forEach((k, v) => print('$k: $v'));
    }
    if (options.data != null) {
      print("Body: ${options.data}");
    }
    print(
        "--> END ${options.method != null ? options.method.toUpperCase() : 'METHOD'}");

    return options;
  }

  @override
  Future<FutureOr> onError(DioError dioError) async {
    print(
        "<-- ${dioError.message} ${(dioError.response?.request != null ? (dioError.response.request.baseUrl + dioError.response.request.path) : 'URL')}");
    print(
        "${dioError.response != null ? dioError.response.data : 'Unknown Error'}");
    print("<-- End error");
  }

  @override
  Future<FutureOr> onResponse(dio.Response response) async {
    print(
        "<-- ${response.statusCode} ${(response.request != null ? (response.request.baseUrl + response.request.path) : 'URL')}");
    print("Headers:");
    response.headers?.forEach((k, v) => print('$k: $v'));
    print("Response: ${response.data}");
    print("<-- END HTTP");
  }
}

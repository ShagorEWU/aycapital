import 'package:aycapital/API/Provider/RegistrationApiProvider.dart';
import 'package:get/get.dart';

class RegistrationRepository {
  RegistrationApiProvider registrationApiProvider = Get.find();

  checkRegistration(Map<String, dynamic> map) {
    return registrationApiProvider.checkRegistration(map);
  }
}

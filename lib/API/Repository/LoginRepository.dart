import 'package:aycapital/API/Provider/LoginApiProvider.dart';
import 'package:get/get.dart';

class LoginRepository {
  LoginApiProvider loginApiProvider = Get.find();

  checkLogin(Map<String, dynamic> map) {
    return loginApiProvider.checkLogin(map);
  }
}

import 'dart:io';

import 'package:aycapital/utils/widgets/another_sample_data.dart';
import 'package:aycapital/utils/widgets/bottom_nav_bar.dart';
import 'package:aycapital/utils/widgets/hexcolor.dart';
import 'package:aycapital/utils/widgets/simple_bar_chart.dart';
import 'package:aycapital/utils/widgets/simple_bar_chart_another.dart';
import 'package:aycapital/utils/widgets/simple_chart_sample222.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ChartShow extends StatefulWidget {
  ChartShow({Key key}) : super(key: key);

  @override
  _ChartShowState createState() => _ChartShowState();
}

class _ChartShowState extends State<ChartShow> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: Platform.isAndroid
          ? SystemUiOverlayStyle.dark.copyWith(
              // To make Status bar icons color white in Android devices.
              statusBarIconBrightness: Brightness.light,
              statusBarColor: HexColor("#121212"),
              systemNavigationBarColor: Colors.black12,
              systemNavigationBarIconBrightness: Brightness.dark,
            )
          : SystemUiOverlayStyle.dark.copyWith(
              // statusBarBrightness is used to set Status bar icon color in iOS.
              statusBarBrightness: Brightness.light,
              statusBarColor: HexColor("#121212"),
              systemNavigationBarColor: Colors.black12,
              systemNavigationBarIconBrightness: Brightness.dark,
            ),
      child: WillPopScope(
        onWillPop: () {
          // ignore: unnecessary_statements
          if (Platform.isAndroid) {
            SystemNavigator.pop();
          } else if (Platform.isIOS) {
            exit(0);
          }
        },
        child: SafeArea(
          top: true,
          child: Scaffold(
              backgroundColor: HexColor("#121212"),
              appBar: AppBar(
                brightness: Brightness.light,
                backgroundColor: HexColor("#121212"),
                leading: Text(""),
                centerTitle: true,
                title: Text(
                  "Dashboard",
                  style: TextStyle(color: Colors.white, fontSize: 22),
                ),
              ),
              body: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(14, 16, 14, 0),
                  child: Column(
                    children: [
                      // Padding(padding: const EdgeInsets.all(8)),
                      LineChartSample2(),
                      Padding(padding: const EdgeInsets.all(7)),
                      LineChartSample222(),
                      Padding(padding: const EdgeInsets.all(7)),
                      LineChartSample22(),
                      Padding(padding: const EdgeInsets.all(7)),
                      BarChartSample3(),
                      Padding(padding: const EdgeInsets.all(7)),
                    ],
                  ),
                ),
              ),
              bottomNavigationBar: BottomNavBar(
                activeBar: 0,
              )),
        ),
      ),
    );
  }
}

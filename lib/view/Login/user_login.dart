import 'dart:collection';
import 'dart:io';

import 'package:aycapital/utils/my_routes.dart';
import 'package:aycapital/utils/widgets/hexcolor.dart';
import 'package:aycapital/view/Login/Block/login_block.dart';
import 'package:aycapital/view/Login/Block/registration_block.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';

GoogleSignIn _googleSignIn = GoogleSignIn(scopes: ['profile', 'email']);

class UserLogin extends StatefulWidget {
  UserLogin({Key key}) : super(key: key);

  @override
  _UserLoginState createState() => _UserLoginState();
}

class _UserLoginState extends State<UserLogin> {
  GoogleSignInAccount _currentUser;

  TextEditingController loginEmail = new TextEditingController();
  TextEditingController loginPassWord = new TextEditingController();
  TextEditingController registrationEmail = new TextEditingController();
  TextEditingController registrationPassWord = new TextEditingController();
  TextEditingController registrationName = new TextEditingController();
  final LoginBloc _loginBloc = Get.put(LoginBloc());

  String device;
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  Map<String, dynamic> _deviceData = <String, dynamic>{};

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount account) {
      setState(() {
        _currentUser = account;
      });
    });
    _googleSignIn.signInSilently();
    initPlatformState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: Platform.isAndroid
          ? SystemUiOverlayStyle.dark.copyWith(
              // To make Status bar icons color white in Android devices.
              statusBarIconBrightness: Brightness.light,
              statusBarColor: HexColor("#121212"),
              systemNavigationBarColor: Colors.black12,
              systemNavigationBarIconBrightness: Brightness.dark,
            )
          : SystemUiOverlayStyle.dark.copyWith(
              // statusBarBrightness is used to set Status bar icon color in iOS.
              statusBarBrightness: Brightness.light,
              statusBarColor: HexColor("#121212"),
              systemNavigationBarColor: Colors.black12,
              systemNavigationBarIconBrightness: Brightness.dark,
            ),
      child: SafeArea(
        top: true,
        child: WillPopScope(
          onWillPop: () {
            if (Platform.isAndroid) {
              SystemNavigator.pop();
            } else if (Platform.isIOS) {
              exit(0);
            }
          },
          child: SafeArea(
            top: true,
            child: Scaffold(
              backgroundColor: HexColor("#121212"),
              body: ListView(
                children: [
                  Padding(
                    padding: const EdgeInsets.fromLTRB(38, 38, 38, 0),
                    child: Column(
                      children: [
                        Padding(padding: const EdgeInsets.all(30)),
                        Center(
                          child: CircleAvatar(
                            radius: 40,
                            backgroundImage: AssetImage("asset/logo.jpg"),
                          ),
                        ),
                        Padding(padding: const EdgeInsets.all(3)),

                        Padding(padding: const EdgeInsets.all(8)),
                        Align(
                          alignment: Alignment.bottomLeft,
                          child: Text(
                            "Email",
                            style: TextStyle(color: Colors.white, fontSize: 16),
                          ),
                        ),
                        Padding(padding: const EdgeInsets.all(3)),
                        Container(
                          width: MediaQuery.of(context).size.width - 76,
                          height: 50,
                          child: TextField(
                            controller: loginEmail,
                            style: TextStyle(color: Colors.white, fontSize: 16),
                            decoration: InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                      color: Colors.white, width: 2.0),
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                fillColor: HexColor("#232323"),
                                filled: true,
                                hintText: "youremail@gmail.com",
                                hintStyle: TextStyle(
                                    color: HexColor("#939393"), fontSize: 16),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide(
                                      color: Colors.white, width: 1.0),
                                )),
                          ),
                        ),

                        Padding(padding: const EdgeInsets.all(8)),
                        Align(
                          alignment: Alignment.bottomLeft,
                          child: Text(
                            "Password",
                            style: TextStyle(color: Colors.white, fontSize: 16),
                          ),
                        ),
                        Padding(padding: const EdgeInsets.all(3)),
                        Container(
                          width: MediaQuery.of(context).size.width - 76,
                          height: 50,
                          child: TextField(
                            controller: loginPassWord,
                            style: TextStyle(color: Colors.white, fontSize: 16),
                            decoration: InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                  borderSide: const BorderSide(
                                      color: Colors.white, width: 2.0),
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                fillColor: HexColor("#232323"),
                                filled: true,
                                hintText: "**********",
                                hintStyle: TextStyle(
                                    color: HexColor("#939393"), fontSize: 16),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide: BorderSide(
                                      color: HexColor("#232323"),
                                      width: 1.0,
                                      style: BorderStyle.solid),
                                )),
                          ),
                        ),
                        Align(
                          alignment: Alignment.topRight,
                          child: TextButton(
                            onPressed: () {},
                            child: Text("Forgot password ?",
                                style: TextStyle(
                                    color: Colors.orange, fontSize: 16)),
                          ),
                        ),
                        //Padding(padding: const EdgeInsets.all(10)),
                        GestureDetector(
                          onTap: () {
                            if (loginEmail.text.isEmpty ||
                                loginPassWord.text.isEmpty) {
                            } else {
                              //print(_loginBloc.emailTextController.text);
                              Map<String, dynamic> map = new HashMap();
                              map["email"] = loginEmail.text.toString();
                              map["password"] = loginPassWord.text.toString();

                              map["device_name"] = "android";
                              print(device);

                              _loginBloc.submitLoginData(map);
                            }
                            //Navigator.pushNamed(context, "/otp");
                          },

                          // Get.toNamed(MyRoutes.chart_show);
                          //Get.toNamed(MyRoutes.user_profile);
                          // },

                          child: Container(
                            decoration: BoxDecoration(
                              color: HexColor("#232323"),
                              borderRadius: BorderRadius.circular(20),
                            ),
                            // color: Colors.blue,
                            width: MediaQuery.of(context).size.width - 76,
                            height: 50,
                            child: Center(
                                child: Text("Login",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 16))),
                          ),
                        ),
                        Padding(padding: const EdgeInsets.all(7)),
                        Center(
                          child: Text("Or",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 16)),
                        ),
                        Padding(padding: const EdgeInsets.all(7)),
                        GestureDetector(
                          onTap: () {
                            _handleSignIn();
                            if (_currentUser != null) {
                              Get.defaultDialog(
                                  content: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  ListTile(
                                    leading: GoogleUserCircleAvatar(
                                      identity: _currentUser,
                                    ),
                                    title: Text(_currentUser.displayName ?? ''),
                                    subtitle: Text(_currentUser.email ?? ''),
                                  ),
                                  RaisedButton(
                                    onPressed: () {
                                      _handleSignOut();
                                      Get.back();
                                    },
                                    // _handleSignOut,
                                    child: Text('Sign Out'),
                                  ),
                                  RaisedButton(
                                    onPressed: () {
                                      Get.toNamed(MyRoutes.chart_show);
                                    },
                                    // _handleSignOut,
                                    child: Text('Continue'),
                                  )
                                ],
                              ));
                            }

                            // GoogleSignIn();
                          },
                          child: new Container(
                              width: MediaQuery.of(context).size.width - 76,
                              height: 50,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Padding(padding: const EdgeInsets.all(20)),
                                  Icon(
                                    MaterialCommunityIcons.google,
                                    size: 25,
                                    color: Colors.white,
                                  ),
                                  Padding(padding: const EdgeInsets.all(10)),
                                  Text("Login with Google",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 16))
                                ],
                              ),
                              decoration: new BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: HexColor("#232323"),
                              )),
                        ),
                        Padding(padding: const EdgeInsets.all(19)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Don't have an account ?",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16)),
                            TextButton(
                              onPressed: () {
                                final RegistrationBlockk _registrationBloc =
                                    Get.put(RegistrationBlockk());
                                Get.defaultDialog(
                                    backgroundColor: HexColor("#121212"),
                                    title: "Registration",
                                    titleStyle: TextStyle(
                                        color: Colors.white, fontSize: 16),
                                    content: Container(
                                      width: MediaQuery.of(context).size.width -
                                          20,
                                      child: Column(
                                        children: [
                                          Padding(
                                              padding: const EdgeInsets.all(8)),
                                          Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Text(
                                              "UserName",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 16),
                                            ),
                                          ),
                                          Padding(
                                              padding: const EdgeInsets.all(3)),
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width -
                                                76,
                                            height: 50,
                                            child: TextField(
                                              controller: registrationName,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 16),
                                              decoration: InputDecoration(
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                    borderSide:
                                                        const BorderSide(
                                                            color: Colors.white,
                                                            width: 2.0),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20.0),
                                                  ),
                                                  fillColor:
                                                      HexColor("#232323"),
                                                  filled: true,
                                                  hintText: "YourName",
                                                  hintStyle: TextStyle(
                                                      color:
                                                          HexColor("#939393"),
                                                      fontSize: 16),
                                                  border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20),
                                                    borderSide: BorderSide(
                                                        color:
                                                            HexColor("#232323"),
                                                        width: 1.0,
                                                        style:
                                                            BorderStyle.solid),
                                                  )),
                                            ),
                                          ),
                                          Padding(
                                              padding: const EdgeInsets.all(8)),
                                          Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Text(
                                              "Password",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 16),
                                            ),
                                          ),
                                          Padding(
                                              padding: const EdgeInsets.all(3)),
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width -
                                                76,
                                            height: 50,
                                            child: TextField(
                                              controller: registrationEmail,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 16),
                                              decoration: InputDecoration(
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                    borderSide:
                                                        const BorderSide(
                                                            color: Colors.white,
                                                            width: 2.0),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20.0),
                                                  ),
                                                  fillColor:
                                                      HexColor("#232323"),
                                                  filled: true,
                                                  hintText: "someone@gmail.com",
                                                  hintStyle: TextStyle(
                                                      color:
                                                          HexColor("#939393"),
                                                      fontSize: 16),
                                                  border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20),
                                                    borderSide: BorderSide(
                                                        color:
                                                            HexColor("#232323"),
                                                        width: 1.0,
                                                        style:
                                                            BorderStyle.solid),
                                                  )),
                                            ),
                                          ),
                                          Padding(
                                              padding: const EdgeInsets.all(8)),
                                          Align(
                                            alignment: Alignment.bottomLeft,
                                            child: Text(
                                              "Password",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 16),
                                            ),
                                          ),
                                          Padding(
                                              padding: const EdgeInsets.all(3)),
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width -
                                                76,
                                            height: 50,
                                            child: TextField(
                                              controller: registrationPassWord,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 16),
                                              decoration: InputDecoration(
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                    borderSide:
                                                        const BorderSide(
                                                            color: Colors.white,
                                                            width: 2.0),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20.0),
                                                  ),
                                                  fillColor:
                                                      HexColor("#232323"),
                                                  filled: true,
                                                  hintText: "**********",
                                                  hintStyle: TextStyle(
                                                      color:
                                                          HexColor("#939393"),
                                                      fontSize: 16),
                                                  border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            20),
                                                    borderSide: BorderSide(
                                                        color:
                                                            HexColor("#232323"),
                                                        width: 1.0,
                                                        style:
                                                            BorderStyle.solid),
                                                  )),
                                            ),
                                          ),
                                          Padding(
                                              padding: const EdgeInsets.all(8)),
                                          GestureDetector(
                                            onTap: () {
                                              if (registrationEmail
                                                      .text.isEmpty ||
                                                  registrationPassWord
                                                      .text.isEmpty ||
                                                  registrationName
                                                      .text.isEmpty) {
                                              } else {
                                                //print(_loginBloc.emailTextController.text);
                                                Map<String, dynamic> map =
                                                    new HashMap();
                                                map["name"] = registrationName
                                                    .text
                                                    .toString();
                                                map["email"] = registrationEmail
                                                    .text
                                                    .toString();
                                                map["password"] =
                                                    registrationPassWord.text
                                                        .toString();

                                                map["device_name"] = device;
                                                print(device);

                                                _registrationBloc
                                                    .submitRegistrationData(
                                                        map);
                                              }
                                              //Navigator.pushNamed(context, "/otp");
                                            },

                                            // Get.toNamed(MyRoutes.chart_show);
                                            //Get.toNamed(MyRoutes.user_profile);
                                            // },

                                            child: Container(
                                              decoration: BoxDecoration(
                                                color: HexColor("#232323"),
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                              ),
                                              // color: Colors.blue,
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width -
                                                  76,
                                              height: 50,
                                              child: Center(
                                                  child: Text("Registration",
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 16))),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ));
                              },
                              child: Text(" Registration",
                                  style: TextStyle(
                                      color: Colors.orange, fontSize: 16)),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildBody() {}

  Future<void> _handleSignIn() async {
    await _googleSignIn.signIn().then((result) {
      result.authentication.then((googleKey) {
        print("Access");
        print(googleKey.accessToken);
        if (googleKey.idToken.toString() ==
            "eyJhbGciOiJSUzI1NiIsImtpZCI6IjZhOGJhNTY1MmE3MDQ0MTIxZDRmZWRhYzhmMTRkMTRjNTRlNDg5NWIiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiI1MzAzNTQ5NDQyMzktZDc3b2hkaDhsZjBydmFqa2djYzBrZ2gzZGtsMGJxbzMuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI1MzAzNTQ5NDQyMzktbjNjdXF0M3NnZjZjcTVxMzY3ZGFiMW0zbGhlOHFoaDQuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMTc1NTIyOTIzNTg2NzMwNzQ3MjkiLCJlbWFpbCI6InNoYWdvcm5vbWFuQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJuYW1lIjoiQWJkdWxsYWggQWwgTm9tYW4iLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tL2EtL0FPaDE0R2pObGFnWGJvRWpHblkzY0oyaDhGNFBOV1RiR1czQmhBcllSeFkzPXM5Ni1jIiwiZ2l2ZW5fbmFtZSI6IkFiZHVsbGFoIEFsIiwiZmFtaWx5X25hbWUiOiJOb21hbiIsImxvY2FsZSI6ImVuLUdCIiwiaWF0IjoxNjE2NDAzNDI3LCJleHAiOjE2MTY0MDcwMjd9.ahJmx0CTtreLZDyjugRT5e6XpaOkuxZYyuYUM8M-Ujzmf838cIkg7YG5Lbwk_TkOH3TrZKBu92xpfbGqU4aJfnsDyFDQQ-yCTnOUt9gtekj4CDwQCUSGng_P_1X6M_yf7HA-wFYetZqYgRMqcXV7eypWgj5AfzeaOCqpFIif5MkCKXtAs37ziBg7tNhw82iebitvPLvAGCEFncC2h5OnqjUR6YG0FoMfmnh6C-ZPzuZVJuC") {
          print("yes");
        } else {
          print("No");
        }
        print("ID");
        print(googleKey.idToken);

        print("Name");
        print(_googleSignIn.currentUser.displayName);
      }).catchError((err) {
        print('inner error');
      });
    }).catchError((err) {
      print('error occured');
    });
  }

  Future<void> _handleSignOut() async {
    _googleSignIn.disconnect();
  }

  Future<void> initPlatformState() async {
    Map<String, dynamic> deviceData;
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

    try {
      if (Platform.isAndroid) {
        AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
        device = androidInfo.device;
        print('Running on${androidInfo.device}M');
      } else if (Platform.isIOS) {
        IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
        print('Running on ${iosInfo.utsname.machine}');
      }
    } on PlatformException {}
  }
}

import 'package:aycapital/API/Repository/RegistrationRepository.dart';
import 'package:aycapital/Model/RegistrationModel/RegistrationModel.dart';
import 'package:aycapital/utils/Notifications.dart';
import 'package:aycapital/utils/Storage/storagePref.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';

class RegistrationBlockk extends GetxController {
  //final c = Get.put(UserController());
  RegistrationRepository repository = Get.find();

  StoragePref storagePref = Get.find();

  Notifications notifications = Get.find();
  //bool isAuthinticated;
  @override
  void onInit() {
    super.onInit();
    //isAuthinticated = false;
  }

  void submitRegistrationData(Map<String, dynamic> map) async {
    notifications.Loader(msg: "Registration...");

    RegistrationModel response = await repository.checkRegistration(map);

    if (response.success == true) {
      // storagePref.storageWrite("email", response.data[0].email);
      // storagePref.storageWrite("phoneNumber", response.data[0].phoneNumber);
      // storagePref.storageWrite("userName", response.data[0].adminName);

      notifications.dismissLoader();
      Get.back();
    } else {
      notifications.dismissLoader();

      notifications.snackBarDialog(
          message: "Invalid Input",
          icon: FlutterIcons.warning_faw,
          position: SnackPosition.TOP);
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}

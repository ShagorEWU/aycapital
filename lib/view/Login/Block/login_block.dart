import 'package:aycapital/API/Repository/LoginRepository.dart';
import 'package:aycapital/Model/loginModel/LoginModel.dart';
import 'package:aycapital/utils/Notifications.dart';
import 'package:aycapital/utils/Storage/storagePref.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';

import '../../../utils/my_routes.dart';

class LoginBloc extends GetxController {
  //final c = Get.put(UserController());
  LoginRepository repository = Get.find();

  StoragePref storagePref = Get.find();

  Notifications notifications = Get.find();
  //bool isAuthinticated;
  @override
  void onInit() {
    super.onInit();
    //isAuthinticated = false;
  }

  void submitLoginData(Map<String, dynamic> map) async {
    notifications.Loader(msg: "Signing IN");

    LoginModel response = await repository.checkLogin(map);

    if (response.success == true) {
      // storagePref.storageWrite("email", response.data[0].email);
      // storagePref.storageWrite("phoneNumber", response.data[0].phoneNumber);
      // storagePref.storageWrite("userName", response.data[0].adminName);

      notifications.dismissLoader();
      Get.toNamed(MyRoutes.chart_show);
    } else {
      notifications.dismissLoader();

      notifications.snackBarDialog(
          message: "Invalid Input",
          icon: FlutterIcons.warning_faw,
          position: SnackPosition.TOP);
    }
  }

  @override
  void onClose() {
    super.onClose();
  }
}

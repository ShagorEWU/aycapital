import 'dart:io';

import 'package:aycapital/utils/widgets/bottom_nav_bar.dart';
import 'package:aycapital/utils/widgets/hexcolor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import 'chart_show.dart';

class UserProfile extends StatefulWidget {
  UserProfile({Key key}) : super(key: key);

  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: Platform.isAndroid
          ? SystemUiOverlayStyle.dark.copyWith(
              // To make Status bar icons color white in Android devices.
              statusBarIconBrightness: Brightness.light,
              statusBarColor: HexColor("#121212"),
              systemNavigationBarColor: Colors.black12,
              systemNavigationBarIconBrightness: Brightness.dark,
            )
          : SystemUiOverlayStyle.dark.copyWith(
              // statusBarBrightness is used to set Status bar icon color in iOS.
              statusBarBrightness: Brightness.light,
              statusBarColor: HexColor("#121212"),
              systemNavigationBarColor: Colors.black12,
              systemNavigationBarIconBrightness: Brightness.dark,
            ),
      child: WillPopScope(
        onWillPop: () {
          Get.to(ChartShow());
        },
        child: SafeArea(
          top: true,
          child: Scaffold(
              appBar: AppBar(
                backgroundColor: HexColor("#121212"),
                leading: Text(""),
                centerTitle: true,
                title: Text(
                  "Profile",
                  style: TextStyle(color: Colors.white, fontSize: 22),
                ),
              ),
              backgroundColor: HexColor("#121212"),
              body: ListView(
                children: [
                  Container(
                    // color:
                    padding: const EdgeInsets.fromLTRB(16, 5, 16, 0),
                    child: Column(
                      children: [
                        //Padding(padding: const EdgeInsets.all(10)),

                        Padding(padding: const EdgeInsets.all(8)),

                        Align(
                          alignment: Alignment.bottomRight,
                          child: TextButton(onPressed: () {}, child: Text("")),
                        ),
                        Padding(padding: const EdgeInsets.all(2)),
                        Container(
                          height: 55,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            color: HexColor("#232323"),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(padding: const EdgeInsets.all(4)),
                              Padding(
                                padding: const EdgeInsets.only(left: 8),
                                child: Text(
                                  "User Name :  ",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 15),
                                ),
                              ),
                              Padding(padding: const EdgeInsets.all(1)),
                              Padding(
                                padding: const EdgeInsets.only(left: 13.0),
                                child: Text(
                                  "< user Name > ",
                                  style: TextStyle(
                                      color: HexColor("#939393"), fontSize: 12),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 16,
                        ),

                        Container(
                          height: 55,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            color: HexColor("#232323"),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(padding: const EdgeInsets.all(4)),
                              Padding(
                                padding: const EdgeInsets.only(left: 10),
                                child: Text(
                                  "Email :  ",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 15),
                                ),
                              ),
                              Padding(padding: const EdgeInsets.all(1)),
                              Padding(
                                padding: const EdgeInsets.only(left: 13.0),
                                child: Text(
                                  "< something@gmail.com > ",
                                  style: TextStyle(
                                      color: HexColor("#939393"), fontSize: 12),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Container(
                          height: 55,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            color: HexColor("#232323"),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(padding: const EdgeInsets.all(4)),
                              Padding(
                                padding: const EdgeInsets.only(left: 10),
                                child: Text(
                                  "Phone :  ",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 15),
                                ),
                              ),
                              Padding(padding: const EdgeInsets.all(1)),
                              Padding(
                                padding: const EdgeInsets.only(left: 13.0),
                                child: Text(
                                  "< 12121212 > ",
                                  style: TextStyle(
                                      color: HexColor("#939393"), fontSize: 12),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Container(
                          height: 55,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            color: HexColor("#232323"),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(padding: const EdgeInsets.all(4)),
                              Padding(
                                padding: const EdgeInsets.only(left: 10),
                                child: Text(
                                  "Address :  ",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 15),
                                ),
                              ),
                              Padding(padding: const EdgeInsets.all(1)),
                              Padding(
                                padding: const EdgeInsets.only(left: 13.0),
                                child: Text(
                                  "< userAddress > ",
                                  style: TextStyle(
                                      color: HexColor("#939393"), fontSize: 12),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              bottomNavigationBar: BottomNavBar(
                activeBar: 3,
              )),
        ),
      ),
    );
  }
}

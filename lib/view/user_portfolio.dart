import 'dart:io';

import 'package:aycapital/utils/widgets/another_new_stack_widget.dart';
import 'package:aycapital/utils/widgets/bottom_nav_bar.dart';
import 'package:aycapital/utils/widgets/hexcolor.dart';
import 'package:aycapital/utils/widgets/new_stack_widget.dart';
import 'package:aycapital/utils/widgets/stack_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import 'chart_show.dart';

class UserProfilePortfolio extends StatefulWidget {
  UserProfilePortfolio({Key key}) : super(key: key);

  @override
  _UserProfilePortfolioState createState() => _UserProfilePortfolioState();
}

class _UserProfilePortfolioState extends State<UserProfilePortfolio> {
  @override
  void initState() {
    // print(MediaQuery.of(context).size.width);
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: Platform.isAndroid
          ? SystemUiOverlayStyle.dark.copyWith(
              // To make Status bar icons color white in Android devices.
              statusBarIconBrightness: Brightness.light,
              statusBarColor: HexColor("#121212"),
              systemNavigationBarColor: Colors.black12,
              systemNavigationBarIconBrightness: Brightness.dark,
            )
          : SystemUiOverlayStyle.dark.copyWith(
              // statusBarBrightness is used to set Status bar icon color in iOS.
              statusBarBrightness: Brightness.light,
              statusBarColor: HexColor("#121212"),
              systemNavigationBarColor: Colors.black12,
              systemNavigationBarIconBrightness: Brightness.dark,
            ),
      child: WillPopScope(
        onWillPop: () {
          Get.to(ChartShow());
        },
        child: SafeArea(
          top: true,
          child: Scaffold(
              backgroundColor: HexColor("#121212"),
              appBar: AppBar(
                backgroundColor: HexColor("#121212"),
                leading: Text(""),
                centerTitle: true,
                title: Text(
                  "My Portfolio",
                  style: TextStyle(fontSize: 22, color: Colors.white),
                ),
              ),
              body: Padding(
                padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
                child: ListView(
                  children: [
                    Container(
                      height: 10,
                      width: MediaQuery.of(context).size.width - 32,
                    ),
                    Container(
                      // padding: const EdgeInsets.all(8),
                      //color: Color.fromRGBO(35, 35, 35, 0.7),
                      // width: (MediaQuery.of(context).size.width),
                      // height: 50,
                      child: Row(
                        children: [
                          Container(
                            width: ((MediaQuery.of(context).size.width - 32) /
                                    4.0) +
                                25.0,
                            child: Center(
                              child: Text(
                                "My Account",
                                style: TextStyle(
                                    fontSize: 12, color: Colors.white),
                              ),
                            ),
                          ),
                          Container(
                            width: ((MediaQuery.of(context).size.width - 32) /
                                    4.0) -
                                25.0,
                            child: Center(
                              child: Text(
                                "Deposit",
                                style: TextStyle(
                                    fontSize: 12, color: Colors.white),
                              ),
                            ),
                          ),
                          Container(
                            width: ((MediaQuery.of(context).size.width - 32) /
                                    4.0) +
                                12,
                            child: Center(
                              child: Text(
                                "Progress",
                                style: TextStyle(
                                    fontSize: 12, color: Colors.white),
                              ),
                            ),
                          ),
                          Container(
                            width: ((MediaQuery.of(context).size.width - 32) /
                                    4.0) -
                                12,
                            child: Center(
                              child: Text(
                                "Status",
                                style: TextStyle(
                                    fontSize: 12, color: Colors.white),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      color: Colors.white,
                      height: 1,
                      width: MediaQuery.of(context).size.width - 32,
                    ),
                    Stack2(),
                    Container(
                      color: Colors.white,
                      height: 1,
                      width: MediaQuery.of(context).size.width - 32,
                    ),
                    Stack3(),
                    Container(
                      color: Colors.white,
                      height: 1,
                      width: MediaQuery.of(context).size.width - 32,
                    ),
                    Stack1(),
                    Container(
                      color: Colors.white,
                      height: 1,
                      width: MediaQuery.of(context).size.width - 32,
                    ),
                    Stack1(),
                    Container(
                      color: Colors.white,
                      height: 1,
                      width: MediaQuery.of(context).size.width - 32,
                    ),
                    Stack1(),
                    Container(
                      color: Colors.white,
                      height: 1,
                      width: MediaQuery.of(context).size.width - 32,
                    ),
                    Stack1(),
                  ],
                ),
              ),
              bottomNavigationBar: BottomNavBar(
                activeBar: 1,
              )),
        ),
      ),
    );
  }
}

import 'dart:io';

import 'package:aycapital/utils/widgets/bottom_nav_bar.dart';
import 'package:aycapital/utils/widgets/hexcolor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

import 'chart_show.dart';

class PercentIndicator extends StatefulWidget {
  PercentIndicator({Key key}) : super(key: key);

  @override
  _PercentIndicatorState createState() => _PercentIndicatorState();
}

class _PercentIndicatorState extends State<PercentIndicator> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: Platform.isAndroid
          ? SystemUiOverlayStyle.dark.copyWith(
              // To make Status bar icons color white in Android devices.
              statusBarIconBrightness: Brightness.light,
              statusBarColor: HexColor("#121212"),
              systemNavigationBarColor: Colors.black12,
              systemNavigationBarIconBrightness: Brightness.dark,
            )
          : SystemUiOverlayStyle.dark.copyWith(
              // statusBarBrightness is used to set Status bar icon color in iOS.
              statusBarBrightness: Brightness.light,
              statusBarColor: HexColor("#121212"),
              systemNavigationBarColor: Colors.black12,
              systemNavigationBarIconBrightness: Brightness.dark,
            ),
      child: WillPopScope(
        onWillPop: () {
          Get.to(ChartShow());
        },
        child: SafeArea(
          top: true,
          child: Scaffold(
              backgroundColor: HexColor("#121212"),
              appBar: AppBar(
                backgroundColor: HexColor("#121212"),
                centerTitle: true,
                leading: Text(""),
                title: Text(
                  "Targets",
                  style: TextStyle(fontSize: 22, color: Colors.white),
                ),
              ),
              body: SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(padding: const EdgeInsets.all(8)),
                    Center(
                      child: Text(
                        "DAILY PROFIT TARGET",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                    Padding(padding: const EdgeInsets.all(8)),
                    new CircularPercentIndicator(
                      radius: 130.0,
                      lineWidth: 10.0,
                      percent: 0.64,
                      center: new Text(
                        "64%",
                        style: TextStyle(color: Colors.white, fontSize: 15),
                      ),
                      progressColor: Colors.blue,
                    ),
                    Padding(padding: const EdgeInsets.all(15)),
                    Center(
                      child: Text(
                        "WEEKLY PROFIT TARGET",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                    Padding(padding: const EdgeInsets.all(8)),
                    new CircularPercentIndicator(
                      radius: 130.0,
                      lineWidth: 10.0,
                      percent: 0.64,
                      center: new Text(
                        "64%",
                        style: TextStyle(color: Colors.white, fontSize: 15),
                      ),
                      progressColor: Colors.blue,
                    ),
                    Padding(padding: const EdgeInsets.all(15)),
                    Center(
                      child: Text(
                        "MONTHLY PROFIT TARGET",
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                    Padding(padding: const EdgeInsets.all(8)),
                    new CircularPercentIndicator(
                      radius: 130.0,
                      lineWidth: 10.0,
                      percent: 0.64,
                      center: new Text(
                        "64%",
                        style: TextStyle(color: Colors.white, fontSize: 15),
                      ),
                      progressColor: Colors.blue,
                    ),
                    Padding(padding: const EdgeInsets.all(15)),
                  ],
                ),
              ),
              bottomNavigationBar: BottomNavBar(
                activeBar: 2,
              )),
        ),
      ),
    );
  }
}
